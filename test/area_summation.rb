#!/usr/bin/env ruby
# coding: utf-8

require 'json'
require 'bigdecimal'

result = {'features' => []}

json_data = open(ARGV[0]) do |data|
   JSON.load(data)
end

crs = json_data['crs']['properties']['name']

chk = 0
(2443..2461).each do |num|
   if crs = "urn:ogc:def:crs:EPSG::#{num.to_s}"
      chk = 1
   end
end

features = json_data['features']


features.each do |feature|

   coordinates = feature['geometry']['coordinates'][0]

   properties = feature['properties']

   s = 0.0

   coordinates.each_with_index do |coord, i|
      break if (coordinates.size - 1) == i
      unless i == 0
         s = s + (
               BigDecimal(coordinates[ i + 1 ][0].to_s) -
               BigDecimal(coordinates[ i - 1 ][0].to_s)
            ) * BigDecimal(coord[1].to_s)
      else
         s = s + (
               BigDecimal(coordinates[1][0].to_s) -
               BigDecimal(coordinates[-2][0].to_s)
            ) * BigDecimal(coord[1].to_s)
      end
   end

   summation = (s / 2).abs.to_f

   properties['summation'] = summation
   result['features'] << properties

end

result_file = ARGV[0].to_s.gsub('.geojson', '.result.json')

new_json = JSON.generate(result)

open(result_file, "w:UTF-8") do |io|
   io.write(new_json)
end