#area_summation

世界測地2000のジオメトリを持つ GeoJSON のポリゴン面積を求めます。  
中抜きポリゴンには対応していません。

```
$ area_summation input.geojson
```

`input.result.json` が出力されます。

中抜け(ドーナツ型)ポリゴンの計算はできません。  
気が向いたら対応するかもしれません。

----
Copyright (c) 2015 Zoar  
[Released under the MIT LICENCE](http://opensource.org/licenses/mit-license.php)
