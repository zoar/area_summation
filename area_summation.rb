#!/usr/bin/env ruby
# coding: utf-8

require 'json'
require 'bigdecimal'

result = {'features' => []}

def CRS_Check(crs)

   chk = 0
   (2443..2461).each do |num|
      if crs == "urn:ogc:def:crs:EPSG::#{num.to_s}"
         chk = 1
      end
   end
   
   if chk == 0 
      puts "CRS Unknown or Out of range : #{crs}"
      exit 1
   end
   
end

def calc_sum(feature)
      coordinates = feature['geometry']['coordinates'][0]
      s = 0.0
      
      coordinates.each_with_index do |coord, i|
         break if (coordinates.size - 1) == i
         unless i == 0
            s = s + (
                  BigDecimal(coordinates[i + 1][0].to_s) -
                  BigDecimal(coordinates[i - 1][0].to_s)
               ) * BigDecimal(coord[1].to_s)
         else
            s = s + (
                  BigDecimal(coordinates[1][0].to_s) -
                  BigDecimal(coordinates[-2][0].to_s)
               ) * BigDecimal(coord[1].to_s)
         end
      end
      
      summation = (s / 2).to_f.abs
end


input_file = ARGV[0].to_s

unless File.exist?(input_file)
   puts "Inaccessible or Not exist."
   exit 1
end

result_file = input_file.gsub('.geojson', '.result.json')

json_data = open(input_file) do |data|
   JSON.load(data)
end

crs = json_data['crs']['properties']['name']

CRS_Check(crs)

features = json_data['features']

features.each do |feature|
   properties = feature['properties']
   
   summation = calc_sum(feature)
   
   properties['summation'] = summation
   result['features'] << properties
end

new_json = JSON.generate(result)

open(result_file, "w:UTF-8") do |io|
   io.write(new_json)
end

exit 0